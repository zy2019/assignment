/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.wordfrequency;

import java.util.List;

/**
 *
 * @author Laptop ZY
 */
public interface WordFrequency {

    static final String WORD_OCCURRENCE_FORMAT = "\"%s\":%d";

    /**
     *
     * @param words user input
     * @throws InvalidInputException
     */
    void readWords(String words) throws InvalidInputException;

    /**
     *
     * @return the list of word Occurrences. Given an example here:
     * <ul>
     * <li>"the":2
     * <li>"moon":1
     * </ul>
     */
    List<String> getWordOccurrences();

}
