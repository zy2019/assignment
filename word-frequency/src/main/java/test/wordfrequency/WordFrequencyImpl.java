/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.wordfrequency;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Laptop ZY
 */
public class WordFrequencyImpl implements WordFrequency {

    private ConcurrentHashMap<String, Integer> wordOccurrences;

    public WordFrequencyImpl() {
        wordOccurrences = new ConcurrentHashMap<String, Integer>();
    }

    @Override
    public void readWords(String words) throws InvalidInputException {
        validateInput(words);
        parseMessage(words);
    }

    @Override
    public List<String> getWordOccurrences() {
        List<String> result = new ArrayList();
        for (Entry<String, Integer> entry : wordOccurrences.entrySet()) {
            result.add(String.format(WORD_OCCURRENCE_FORMAT, entry.getKey().toString(), entry.getValue()));
        }
        return result;
    }

    private void validateInput(String input) throws InvalidInputException {
        if (input == null || input.trim().isEmpty()) {
            throw new InvalidInputException("Empty or Null");
        }
    }

    private void parseMessage(String words) {
        String[] wordArray = words.split("\\s+");
        for (int i = 0; i < wordArray.length; i++) {
            String word = wordArray[i];
            if (wordOccurrences.putIfAbsent(word, 1) != null) {
                wordOccurrences.computeIfPresent(word, (k, v) -> v + 1);
            }
        }
    }
}
