/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.wordfrequency;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
/**
 *
 * @author Laptop ZY
 */
public class WordFrequencyTest {

    private WordFrequency wordFrequency;
    private String input;

    @Before
    public void setUp() {
        wordFrequency = new WordFrequencyImpl();
    }

    @Test(expected = InvalidInputException.class)
    public void readNullShouldReturnInvalidInputException() throws InvalidInputException {
        input = null;
        wordFrequency.readWords(input);
    }

    @Test(expected = InvalidInputException.class)
    public void readEmptyShouldReturnInvalidInputException() throws InvalidInputException {
        input = "    ";
        wordFrequency.readWords(input);
    }

    @Test
    public void getWordOccurrencesShouldReturnExpectedListWhenReadSingleLineInput() throws InvalidInputException {
        input = "the man in the moon";
        wordFrequency.readWords(input);
        List<String> actualResult = wordFrequency.getWordOccurrences();
        assertThat(actualResult, containsInAnyOrder("\"the\":2","\"man\":1", "\"in\":1","\"moon\":1"));
    }
    
    @Test
    public void getWordOccurrencesShouldReturnExpectedListWhenReadInputMutipleTimes() throws InvalidInputException {
        input = "the man in the moon";
        wordFrequency.readWords(input);
        
        input = "the man on the moon";
        wordFrequency.readWords(input);
        List<String> actualResult = wordFrequency.getWordOccurrences();
       assertThat(actualResult, containsInAnyOrder("\"the\":4","\"man\":2", "\"in\":1","\"on\":1","\"moon\":2"));
    }
    
    @Test
    public void concurrentInputtingShouldReturnCorrectResult() throws InterruptedException {
         input = "the man in the moon";
        final CountDownLatch latch = new CountDownLatch(1);
        for (int i = 0; i < 50; ++i) {
            Runnable runner = new Runnable() {
                public void run() {
                    try {
                        latch.await();
                        try {
                            wordFrequency.readWords(input);
                        } catch (InvalidInputException ex) {
                            System.err.println("InvalidInputException : "+ex.getMessage());
                        }
                    } catch (InterruptedException ie) {
                        System.err.println("InterruptedException : "+ie.getMessage());
                    }
                }
            };
             Thread t=new Thread(runner, "TestThread" + i);
             t.start();
             System.out.println("Thread started : "+t.getName());
        }
        latch.countDown(); 
        Thread.sleep(100);
        List<String> actualResult = wordFrequency.getWordOccurrences();
       assertThat(actualResult, containsInAnyOrder("\"the\":100","\"man\":50", "\"in\":50","\"moon\":50"));
    }
    
    



}
