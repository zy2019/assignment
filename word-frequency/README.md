# Word Frequency

## The brief
Please develop code that is both production quality code and fully unit-tested.

All input should be validated to prevent empty or null Strings being processed – appropriate exceptions should be thrown in these scenarios.

The solution should also be thread-safe so that occurrences are correctly updated when concurrently invoked.

## Task 1
Write a solution in WordFrequency interface that counts the number of occurrences of words in given Strings.
For example if the String `the man in the moon` is passed in then the result will contain:
         
        “the”:	2  
        “man”:	1  
        “in”:	1     
        “moon”:	1

You can decide on how the result will be returned.

## Task 2
Write a solution where adding a subsequent String `the man on the moon` to the Task 1 result will aggregate the results.  The final result will be:

        “the”:	4
        “man”:	2
        “in”:	1
        “on”:	1
        “moon”:	2

You can decide on how to do the aggregation.

## The approach
Put solution under main/java and test under test/java.
You are free to use any additional libraries you see fit.